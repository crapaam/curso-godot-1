extends Sprite


var velocidad = 50 # variable numerica
var direccion_x = 1
var direccion_y = 0
var entera = 10 # int
var deci = 10.5 # float


func _physics_process(delta):
	
	# distancia = velocidad * tiempo
	# movimiento = direccion * distancia
	# movimiento = direccion * velocidad * tiempo
	
	position.x += direccion_x * velocidad * delta
	position.y += direccion_y * velocidad * delta
	
	if Input.is_action_just_pressed("ui_up"):
		
		direccion_y = -1
		
	if Input.is_action_just_pressed("ui_down"):
		
		direccion_y = 1
